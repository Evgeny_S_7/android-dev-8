package com.example.kittyapp.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.kittyapp.model.Fact
import com.google.gson.Gson
import java.net.HttpURLConnection
import java.net.URL

class FactService : Service() {
    private val apiUrl = "https://catfact.ninja/facts?limit=20"

    private val broadcastIntent = Intent("com.example.kittyapp")

    private fun fetchFacts() {
        Thread {
            var facts: List<Fact> = emptyList()
            try {
                val url = URL(apiUrl)
                val connection = url.openConnection() as HttpURLConnection
                connection.requestMethod = "GET"
                connection.connectTimeout = 10000
                val response = connection.inputStream.bufferedReader().readText()
                connection.disconnect()
                val json = Gson().fromJson(response, Map::class.java)
                facts = (json["data"] as List<Map<String, Any>>).map {
                    Fact(
                        fact = it["fact"] as String,
                        length = (it["length"] as Double).toInt()
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Thread.sleep(3000)
            broadcastIntent.putExtra("catFacts", Gson().toJson(facts))
            sendBroadcast(broadcastIntent)
        }.start()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        fetchFacts()
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}