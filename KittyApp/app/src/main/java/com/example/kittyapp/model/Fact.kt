package com.example.kittyapp.model

data class Fact(
    val fact: String,
    val length: Int
)
