package com.example.kittyapp.viewmodel


import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.kittyapp.model.Fact
import com.example.kittyapp.service.FactService
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val _catFacts = MutableLiveData<List<Fact>>()
    val catFacts: LiveData<List<Fact>> = _catFacts

    fun startService(context: Context) {
        viewModelScope.launch {
            val intent = Intent(context, FactService::class.java)
            context.startService(intent)
        }
    }

    fun startWorkManager(context: Context, request: OneTimeWorkRequest) {
        viewModelScope.launch {
            WorkManager.getInstance(context).enqueueUniqueWork("myWork",ExistingWorkPolicy.REPLACE,request)
        }
    }

    fun updateFacts(catFacts: List<Fact>) {
        _catFacts.value = catFacts
    }
}