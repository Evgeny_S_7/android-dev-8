package com.example.kittyapp.worker

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.kittyapp.model.Fact
import com.google.gson.Gson
import java.net.HttpURLConnection
import java.net.URL

class FactWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    private val apiUrl = "https://catfact.ninja/facts?limit=20"

    @WorkerThread
    override fun doWork(): Result {
        var facts: List<Fact> = emptyList()
        try {
            val url = URL(apiUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.connectTimeout = 10000
            val response = connection.inputStream.bufferedReader().readText()
            connection.disconnect()
            val json = Gson().fromJson(response, Map::class.java)
            facts = (json["data"] as List<Map<String, Any>>).map {
                Fact(
                    fact = it["fact"] as String,
                    length = (it["length"] as Double).toInt()
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Thread.sleep(5000)
        val outputData = Data.Builder()
            .putString("catFacts", Gson().toJson(facts))
            .build()
        return Result.success(outputData)
    }
}